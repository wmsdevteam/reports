<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="mands_ord_man_ords_rec_after_sla" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="814" leftMargin="14" rightMargin="14" topMargin="20" bottomMargin="20" uuid="c79edc78-ceb9-4233-aaf0-1d26fa6751d9">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:/Users/P9125619/Documents/reports"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="site_id" class="java.lang.String"/>
	<parameter name="client_id" class="java.lang.String"/>
	<parameter name="order_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="order_type" class="java.lang.String"/>
	<queryString>
		<![CDATA[--------------------------------------------------------------------
-- Order Management - Orders Received after agreed SLA            --
--                                                                --
-- Version  Author     Ref      Date      Detail                  --
-- -------  ---------- -------- --------- --------------------------
-- 1.0      M.Russ     JDA-2073 30/08/16  Initial Version         --
--          Mark Reed  JDA-3163 04/01/17  Formatting issues       --
--------------------------------------------------------------------
SELECT
  TRUNC(oh.creation_date) "ORDER DATE",
  TO_CHAR(oh.creation_date,'hh24:mi:ss') "ORDER TIME",
  oh.order_type,
  InitCap(oh.name) name,
  s.product_group "DEPARTMENT",
  oh.order_id,
  oh.customer_id "DESTINATION",
  TRUNC(oh.ship_by_date) "EXPECTED DISPATCH DATE",
  SUM(ol.qty_ordered) "VOLUME SINGLES"
FROM order_header oh,
  order_line ol,
  sku s
WHERE oh.order_id = ol.order_id
AND oh.client_id  = ol.client_id
AND ol.client_id  = s.client_id
AND ol.sku_id     = s.sku_id
AND (oh.creation_date BETWEEN to_date($P{order_date}, 'DD/MM/YYYY')||' 05.00'
/**************************************************/
--CUT-OFF TIME TO BE DEFINED
AND TO_DATE($P{order_date} ||' 05.00', 'DD/MM/YYYY HH24:MI') + interval '1' day OR $P{order_date} IS NULL)
/**************************************************/
AND (oh.order_type = $P{order_type} or $P{order_type} is null)
AND (oh.from_site_id = $P{site_id} or $P{site_id} is null)
AND (oh.client_id = $P{client_id} or $P{client_id} is null)
GROUP BY
  oh.creation_date,
  oh.order_type,
  s.product_group,
  oh.order_id,
  oh.customer_id,
  oh.name,
  oh.ship_by_date
ORDER BY 1, 2, 3, 4, 5]]>
	</queryString>
	<field name="ORDER DATE" class="java.sql.Timestamp"/>
	<field name="ORDER TIME" class="java.lang.String"/>
	<field name="ORDER_TYPE" class="java.lang.String"/>
	<field name="NAME" class="java.lang.String"/>
	<field name="DEPARTMENT" class="java.lang.String"/>
	<field name="ORDER_ID" class="java.lang.String"/>
	<field name="DESTINATION" class="java.lang.String"/>
	<field name="EXPECTED DISPATCH DATE" class="java.sql.Timestamp"/>
	<field name="VOLUME SINGLES" class="java.math.BigDecimal"/>
	<variable name="StoreTotal" class="java.math.BigDecimal" resetType="Group" resetGroup="Store" calculation="Sum">
		<variableExpression><![CDATA[$F{VOLUME SINGLES}]]></variableExpression>
	</variable>
	<group name="Store">
		<groupExpression><![CDATA[$F{NAME}]]></groupExpression>
		<groupFooter>
			<band height="20">
				<textField>
					<reportElement style="NOSTYLE" x="704" y="0" width="110" height="20" uuid="4a60b28d-04df-42e7-b0cc-9123fab5580c"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{StoreTotal}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement style="NOSTYLE" x="360" y="0" width="332" height="20" uuid="588b8bcd-2c37-4222-b8b3-5650d88c8264"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{NAME} + " Total:"]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="61" splitType="Stretch">
			<rectangle>
				<reportElement style="rptHeaderRectangle" x="0" y="0" width="814" height="40" uuid="307222ac-4465-4777-afa4-35ba6363814e"/>
			</rectangle>
			<staticText>
				<reportElement style="rptTitle" x="0" y="0" width="814" height="40" uuid="b52515f5-e826-4b6f-81d7-ddb3ad7ebdc7"/>
				<text><![CDATA[Orders Received after SLA]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="50" splitType="Stretch">
			<staticText>
				<reportElement style="rptHeader" x="0" y="0" width="55" height="50" uuid="10ffa875-d790-4037-aa70-f2522b892cf2"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Order
Date	]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="55" y="0" width="60" height="50" uuid="2d77b79b-2320-414c-84dd-db4169f34d23"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Order
Time]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="115" y="0" width="60" height="50" uuid="380445ba-8c42-48ec-8df5-8838e522b641"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Order
Type]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="285" y="0" width="75" height="50" uuid="56ce0ee8-e8f6-48d3-bb8b-314ca7f6de2b"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Department]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="360" y="0" width="110" height="50" uuid="0009b267-2297-4f4c-a41e-fc299d8c8b48"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Order Reference]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="470" y="0" width="110" height="50" uuid="b9a79ea2-0956-4280-a1ba-5d4820a16537"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Destination]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="580" y="0" width="124" height="50" uuid="69e27868-a3ff-4fbb-a5f2-2396a4d21946"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Expected Dispatch Date]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="704" y="0" width="110" height="50" uuid="219fecea-6bf4-4eeb-9aac-0bc2927668c4"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Quantity (Singles)]]></text>
			</staticText>
			<staticText>
				<reportElement style="rptHeader" x="175" y="0" width="110" height="50" uuid="21d1e96b-f4b7-45e5-9187-856535a77496"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Store]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement x="0" y="0" width="814" height="20" forecolor="#E9E9E9" backcolor="#E9E9E9" uuid="0759d88e-2540-4947-a65a-6a080b2ede1c">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() %2 == 0)]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="0" y="0" width="55" height="20" uuid="46ab6797-5f89-4222-8af8-d93c669ac4b8"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ORDER DATE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="55" y="0" width="60" height="20" uuid="817ee272-0217-4348-8ab8-ec8f351b8e6c"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ORDER TIME}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="115" y="0" width="60" height="20" uuid="461cc3c6-a94e-44bc-882d-d1b0c4f3e726"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ORDER_TYPE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="285" y="0" width="75" height="20" uuid="111b9676-b140-4aee-8269-5cd26cf53e22"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{DEPARTMENT}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="360" y="0" width="110" height="20" uuid="68976972-69ab-4549-b894-640544d2b00c"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ORDER_ID}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="470" y="0" width="110" height="20" uuid="270c470a-cbf2-48d6-96d6-926e716f57ad"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{DESTINATION}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="580" y="0" width="124" height="20" uuid="235100f5-ba3e-4672-a8f6-b440de25044c"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{EXPECTED DISPATCH DATE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="704" y="0" width="110" height="20" uuid="03735574-e2c6-4539-a079-30102c7d50cb"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{VOLUME SINGLES}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="rptDetailText" x="175" y="0" width="110" height="20" uuid="25bda7fd-4e13-4585-a2e4-c1bb12adf4e1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{NAME}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="NOSTYLE" x="488" y="4" width="42" height="20" uuid="5fd6c41b-3d4c-49b7-8a1b-ec37937e753a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="NOSTYLE" x="285" y="4" width="202" height="20" uuid="479a2e7f-9401-4305-ac45-d95a252d2428"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " +  $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
