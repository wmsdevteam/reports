/******************************************************************************************/
/*                                                                                        */
/*  NAME:           mands_stock_theft_and_loss.sql                                        */
/*                                                                                        */
/*  DESCRIPTION:  Returns all stock adjustments for a given site and client. It is        */
/*                written to be executed as a sql*plus report so the report must be set   */
/*                up this way! This will aloow easy copy and pasting from the web client  */
/*                                                                                        */
/*   DATE     REFERENCE   BY          DESCRIPTION                                         */
/*   ======== =========== =========== ================================================    */
/*   16/01/17 JDA-3374    Mark Reed   Initial version                                     */
/******************************************************************************************/


/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Stock Teft and Loss' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN data HEADING 'Data' FORMAT A132

/* SQL Select statement */

SELECT 'site_id,sku_id,product_group,description,stroke,stroke_description,net,gross' data
from dual
union
SELECT itl.site_id||','||
       itl.sku_id||','||
       s.product_group||',"'||
       s.description||'","'||
       s.user_def_type_1||'","'||
       s.user_def_type_2||'",'||
       SUM(ABS(itl.update_qty))||','||
       SUM(itl.update_qty) data
FROM inventory_transaction itl,
     sku s
WHERE itl.client_id = s.client_id
      AND itl.sku_id = s.sku_id
      AND itl.code = 'Adjustment'
      AND itl.client_id = '&2'
      AND itl.site_id = '&3'
      AND itl.dstamp >= CASE WHEN EXTRACT (HOUR FROM systimestamp) >=6 THEN TRUNC(SYSDATE) + INTERVAL '6' HOUR ELSE TRUNC(SYSDATE -1) + INTERVAL '6' HOUR END
GROUP BY itl.site_id,
         itl.sku_id,
         s.product_group,
         s.description,
         s.user_def_type_1,
         s.user_def_type_2
ORDER BY 1 desc;
