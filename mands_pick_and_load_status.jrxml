<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="mands_pick_and_load_status" pageWidth="720" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="660" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" isIgnorePagination="true" uuid="c79edc78-ceb9-4233-aaf0-1d26fa6751d9">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="0.75"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" isUnderline="false"/>
	<style name="Arial" fontName="Arial" isUnderline="false"/>
	<subDataset name="Picking" uuid="005d742d-b6e2-4b31-a2b6-d5055d5b0848">
		<parameter name="site_id" class="java.lang.String"/>
		<parameter name="client_id" class="java.lang.String"/>
		<queryString>
			<![CDATA[SELECT CASE WHEN level = 1 THEN 'Outstanding' ELSE
         CASE WHEN level = 2 THEN 'Picked' ELSE 'Scheduled' END END typ,
       CASE WHEN level = 1 THEN qty_ordered ELSE
          CASE WHEN level = 2 THEN qty_picked ELSE scheduled_qty END END val
from (
SELECT s2.site_id,
       SUM(ol.qty_ordered) qty_ordered,
       SUM(CASE WHEN S2.due_dstamp <= systimestamp + interval '1' hour THEN ol.qty_ordered ELSE 0 END) scheduled_qty,
       SUM(CASE WHEN S2.due_dstamp <= systimestamp + interval '1' hour THEN NVL(mt.qty_picked,0) + NVL(ol.qty_picked,0) ELSE 0 END) qty_picked
FROM ( SELECT s1.site_id,
              LibDockScheduler.QuerySlotDateTime(s1.site_id,
                                                s1.due_dstamp,
                                                s1.start_slot) due_dstamp,
              s1.type,
              s1.reference_id
       FROM (SELECT bid.site_id,
                    bid.due_dstamp,
                    bid.start_slot,
                    bid.no_slots,
                    bidd.type,
                    bidd.reference_id
             FROM booking_in_diary bid,
                  booking_in_diary_details bidd
             WHERE     bid.bookref_id = bidd.bookref_id
                   AND bid.booking_type = 'Out'
                   AND bid.due_dstamp between TRUNC(sysdate) - 1 AND TRUNC(sysdate) + 1
                   AND bid.site_id = $P{site_id}
            ) S1
       WHERE LibDockScheduler.QuerySlotDateTime(s1.site_id,
                                                s1.due_dstamp,
                                                s1.start_slot) BETWEEN CASE WHEN SYSDATE > TRUNC (SYSDATE) + INTERVAL '6' HOUR THEN TRUNC (SYSDATE) + INTERVAL '6' HOUR ELSE TRUNC (SYSDATE - 1) + INTERVAL '6' HOUR END
                                                                   AND CASE WHEN SYSDATE > TRUNC (SYSDATE) + INTERVAL '6' HOUR THEN TRUNC (SYSDATE) + INTERVAL '30' HOUR ELSE TRUNC (SYSDATE) + INTERVAL '6' HOUR END
     ) S2,
    order_header oh,
    order_line ol,
    (SELECT mt.client_id,
            mt.task_id,
            mt.line_id,
            SUM(CASE WHEN mt.status IN ('Released','Hold','In Progress') THEN mt.qty_to_move ELSE 0 END) qty_to_pick,
            SUM(CASE WHEN mt.status = 'Consol' then mt.qty_to_move ELSE 0 END) qty_picked
     FROM move_task mt
     WHERE mt.client_id = $P{client_id}
           AND mt.site_id = $P{site_id}
           AND mt.task_type in ('O','B')
     GROUP BY mt.client_id,
              mt.task_id,
              mt.line_id
      ) MT
WHERE ((s2.type = 'Consignment' AND s2.reference_id = oh.consignment) OR
       (s2.type = 'Order' AND s2.reference_id = oh.order_id))
       AND oh.client_id = ol.client_id
       AND oh.order_id = ol.order_id
       AND ol.client_id = mt.client_id (+)
       AND ol.order_id = mt.task_id (+)
       AND ol.line_id = mt.line_id (+)
GROUP BY s2.site_id
)
connect by level <= 3]]>
		</queryString>
		<field name="TYP" class="java.lang.String"/>
		<field name="VAL" class="java.math.BigDecimal"/>
	</subDataset>
	<subDataset name="Loading" uuid="764ea2c7-1596-4a34-a014-5e7b5b2d39c6">
		<parameter name="site_id" class="java.lang.String"/>
		<parameter name="client_id" class="java.lang.String"/>
		<queryString>
			<![CDATA[SELECT CASE WHEN level = 1 THEN 'Total' ELSE
        CASE WHEN level = 2 THEN 'scheduled' ELSE
        CASE WHEN Level = 3  THEN  'Loading' ELSE 'Loaded' END END END typ,
       CASE WHEN level = 1 THEN total ELSE
        CASE WHEN level = 2 THEN scheduled ELSE
        CASE WHen Level = 3 Then loading   Else loaded END END END val
FROM (
SELECT COUNT(DISTINCT(trailer_id)) total,
       COUNT(DISTINCT(scheduled)) scheduled,
      -- SUM(loaded) loaded,
      COUNT(DISTINCT(trailer_id)) - COUNT(DISTINCT(scheduled)) - trailer_count loaded,
       trailer_count loading
FROM (SELECT s2.site_id,
       s2.bookref_id||'-'||s2.trailer_id trailer_id,
      -- CASE WHEN S2.due_dstamp <= systimestamp + interval '1' hour THEN s2.bookref_id||'-'||s2.trailer_id END scheduled,
      Case When S2.status='Scheduled' then 1 else 0 End scheduled,
       s2.due_dstamp,
      -- MIN(CASE WHEN oh.status in ('Complete','Shipped','Cancelled') THEN 1 ELSE 0 END) loaded,

       TC.trailer_count
FROM ( SELECT s1.site_id,
              LibDockScheduler.QuerySlotDateTime(s1.site_id,
                                                s1.due_dstamp,
                                                s1.start_slot) due_dstamp,
              s1.type,
              s1.bookref_id,
              s1.trailer_id,
              s1.reference_id,
              s1.status
       FROM (SELECT bid.site_id,
                    bid.bookref_id,
                    bid.due_dstamp,
                    bid.start_slot,
                    bid.no_slots,
                    bidd.type,
                    bidd.reference_id,
                    bid.trailer_id,
                    bid.status
             FROM booking_in_diary bid,
                  booking_in_diary_details bidd
             WHERE     bid.bookref_id = bidd.bookref_id
                   AND bid.booking_type = 'Out'
                   AND bid.due_dstamp between TRUNC(sysdate) - 1 AND TRUNC(sysdate) + 1
                   AND bid.site_id = $P{site_id}
            ) S1
       WHERE LibDockScheduler.QuerySlotDateTime(s1.site_id,
                                                s1.due_dstamp,
                                                s1.start_slot)
            BETWEEN CASE WHEN SYSDATE > TRUNC (SYSDATE) + INTERVAL '6' HOUR THEN TRUNC (SYSDATE) + INTERVAL '6' HOUR ELSE TRUNC (SYSDATE - 1) + INTERVAL '6' HOUR END
                AND CASE WHEN SYSDATE > TRUNC (SYSDATE) + INTERVAL '6' HOUR THEN TRUNC (SYSDATE) + INTERVAL '30' HOUR ELSE TRUNC (SYSDATE) + INTERVAL '6' HOUR END
     ) S2,
    order_header oh,
    (
    select count(distinct sm.location_id) trailer_count from move_task mt,order_header oh,shipping_manifest sm
where  mt.task_id=oh.order_id
and mt.site_id=oh.from_site_id
and mt.client_id=oh.client_id
and oh.consignment=sm.consignment
and oh.from_site_id=sm.site_id
and oh.client_id=sm.client_id
and mt.status!='Complete'
and mt.task_type='O'
and sm.shipped='N'
and mt.site_id=$P{site_id}
and (sm.order_id IN (
                    SELECT  bidd.reference_id
                   FROM booking_in_diary bid,
                   booking_in_diary_details bidd
                   WHERE     bid.bookref_id = bidd.bookref_id
                   AND bid.booking_type = 'Out'
                   AND bid.due_dstamp between TRUNC(sysdate) - 1 AND TRUNC(sysdate) + 1
                   AND bid.site_id = $P{site_id}
                                                 )
OR sm.consignment in (
                    SELECT  bidd.reference_id
                   FROM booking_in_diary bid,
                   booking_in_diary_details bidd
                   WHERE     bid.bookref_id = bidd.bookref_id
                   AND bid.booking_type = 'Out'
                   AND bid.due_dstamp between TRUNC(sysdate) - 1 AND TRUNC(sysdate) + 1
                   AND bid.site_id = $P{site_id}
                                                 ) )

) TC

WHERE ((s2.type = 'Consignment' AND s2.reference_id = NVL(oh.consignment,'Xx')) OR
       (s2.type = 'Order' AND s2.reference_id = oh.order_id))
      AND oh.from_site_id = $P{site_id}
      AND oh.client_id = $P{client_id}
GROUP BY s2.site_id,
       s2.bookref_id||'-'||s2.trailer_id,
       s2.due_dstamp,
       TC.trailer_count,
       S2.status
)
Group By trailer_count
)
CONNECT BY LEVEL <= 4]]>
		</queryString>
		<field name="TYP" class="java.lang.String"/>
		<field name="VAL" class="java.math.BigDecimal"/>
	</subDataset>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:/Users/p9133441/Desktop/ireport files"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="site_id" class="java.lang.String">
		<defaultValueExpression><![CDATA["5885"]]></defaultValueExpression>
	</parameter>
	<parameter name="client_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["M+S"]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:\\Users\\p9133441\\Documents\\jasper\\"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[--------------------------------------------------------------------
-- mands_pick_and_load_status.jasper                              --
--                                                                --
-- Version  Updated by   Date       Detail                        --
-- JDA-2554 Mark Reed    30/08/16   Initial Version               --
-- JDA-3562 G Suliga     02/02/17   Bug fixes                     --
--------------------------------------------------------------------


SELECT TO_CHAR(sysdate,'DD/MM/YYYY') || CHR(10) || TO_CHAR(sysdate,'HH12:MI AM') time_run,
       s.notes,
       nvl(mt.repl_cnt,0) repl_cnt
FROM site s left join  (select nvl(count(mt.key),0) repl_cnt,mt.site_id from  move_task mt where mt.task_type='R'
                       and mt.status='Released'
                       and mt.client_id='M+S'
                       group by mt.site_id) mt
on  s.site_id=mt.site_id
where s.site_id=$P{site_id}]]>
	</queryString>
	<field name="TIME_RUN" class="java.lang.String"/>
	<field name="NOTES" class="java.lang.String"/>
	<field name="REPL_CNT" class="java.math.BigDecimal"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="37" splitType="Stretch">
			<rectangle>
				<reportElement style="rptHeaderRectangle" x="0" y="0" width="659" height="37" uuid="da231acf-2d9d-40eb-a9b8-9ef1ffd94554"/>
			</rectangle>
			<staticText>
				<reportElement style="rptTitle" x="0" y="0" width="659" height="37" uuid="ee53db39-d7cf-45a9-a5e6-35da96c57de0"/>
				<text><![CDATA[Picking/Loading Status]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="70" splitType="Stretch">
			<textField>
				<reportElement x="558" y="33" width="100" height="20" uuid="a83c0622-7d88-4500-9188-8ae8f058ced2"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{NOTES}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="510" y="13" width="148" height="20" uuid="4c3853a2-0564-4ed1-8151-96369059dc31"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{TIME_RUN}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="Arial Unicode" x="369" y="4" width="42" height="20" uuid="5fd6c41b-3d4c-49b7-8a1b-ec37937e753a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="Arial Unicode" x="166" y="4" width="202" height="20" uuid="479a2e7f-9401-4305-ac45-d95a252d2428"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="210" splitType="Stretch">
			<textField>
				<reportElement x="482" y="68" width="177" height="51" uuid="3cdfe967-c748-4fb7-a200-522efbad3b37"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="36" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{REPL_CNT}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="482" y="5" width="177" height="63" uuid="25f094a2-3bd7-4539-aa34-4f5730ae53e0"/>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Outstanding Replens Tasks]]></text>
			</staticText>
			<subreport>
				<reportElement x="0" y="0" width="200" height="68" uuid="dc385d44-c97c-4352-9fec-e3a6007e5541"/>
				<subreportParameter name="site_id">
					<subreportParameterExpression><![CDATA[$P{site_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="client_id">
					<subreportParameterExpression><![CDATA[$P{client_id}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/mand_pick_and_load_status_load_sub.jasper"]]></subreportExpression>
			</subreport>
			<subreport>
				<reportElement x="0" y="108" width="200" height="100" uuid="2e41eaaf-1d9d-4527-8505-ce9010506177"/>
				<subreportParameter name="site_id">
					<subreportParameterExpression><![CDATA[$P{site_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="client_id">
					<subreportParameterExpression><![CDATA[$P{client_id}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/mand_pick_and_load_status_pick_sub.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
