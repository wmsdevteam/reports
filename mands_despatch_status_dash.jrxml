<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="mands_despatch_status_dash" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="c79edc78-ceb9-4233-aaf0-1d26fa6751d9">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="451"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:/Users/P9125619/Documents/reports"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="site_id" class="java.lang.String"/>
	<parameter name="client_id" class="java.lang.String"/>
	<queryString>
		<![CDATA[/******************************************************************************************/
/*                                                                                        */
/*  NAME:         Lib_Extracts                                                            */
/*                                                                                        */
/*  DESCRIPTION:  Package containing all code required to generate M=S extracts           */
/*                                                                                        */
/*   DATE     REFERENCE   BY          DESCRIPTION                                         */
/*   ======== =========== =========== ================================================    */
/*   09/01/17 JDA-2555    Mark Reed   Initial version                                     */
/*   10/01/17 JDA-3314    Mark Reed   Missing column header                               */
/******************************************************************************************/
SELECT S2.*,
       CASE WHEN NVL(shipped_date,systimestamp) > planned_shipment_dstamp THEN 'Delayed' ELSE
         CASE WHEN qty_picked > 0 AND qty_loaded = 0 THEN 'Picking' ELSE
           CASE WHEN qty_to_load > 0 THEN 'Loading' ELSE
             CASE WHEN qty_picked = 0 AND qty_loaded = 0 THEN 'To Start' ELSE
               CASE WHEN shipped_date IS NOT NULL THEN 'DESPATCHED' ELSE '?' END END END END END load_status,

       CASE WHEN NVL(shipped_date,sysdate + interval '1' year) <= planned_shipment_dstamp THEN 'G' ELSE
         CASE WHEN shipped_date IS NULL AND sysdate BETWEEN planned_shipment_dstamp - INTERVAL '60' minute AND planned_shipment_dstamp THEN 'A' ELSE
           CASE WHEN shipped_date IS NOT NULL AND shipped_date > planned_shipment_dstamp THEN 'R' ELSE
             CASE WHEN sysdate <= planned_shipment_dstamp - INTERVAL '61' minute THEN 'B' ELSE '?' END END END END seal_rag,

       CASE WHEN qty_to_pick > 0 AND sysdate >= planned_shipment_dstamp - interval '60' MINUTE THEN 'R' ELSE
         CASE WHEN qty_to_pick > 0 AND (qty_picked + qty_loaded) = 0 THEN 'B' ELSE
           CASE WHEN qty_to_pick = 0 AND last_pick_dstamp <= planned_shipment_dstamp - INTERVAL '60' MINUTE THEN 'G' ELSE '?' END END END pick_rag,

       CASE WHEN qty_loaded > 0 AND sysdate <= planned_shipment_dstamp - INTERVAL '5' MINUTE THEN 'G' ELSE
         CASE WHEN qty_loaded > 0 AND sysdate >= planned_shipment_dstamp - INTERVAL '4' MINUTE THEN 'R' ELSE
           CASE WHEN qty_to_pick > 0 AND qty_loaded = 0 THEN 'B' ELSE '?' END END END load_rag,
       TO_CHAR(planned_shipment_dstamp,'HH24:MI') planned_ship_time,
       TO_CHAR(systimestamp,'DD/MM/YYYY') || CHR(10) || TO_CHAR(systimestamp,'HH12:MI AM') time_run,
       s.notes
FROM
     (   SELECT s1.site_id,
               s1.trailer_id,
               s1.consignment,
               LibDockScheduler.QuerySlotDateTime(s1.site_id, s1.due_dstamp, s1.start_slot) start_time,
               LibDockScheduler.QuerySlotDateTime(s1.site_id, s1.due_dstamp, s1.start_slot) + NUMTODSINTERVAL(LibDockScheduler.GetSlotLength(s1.site_id, s1.due_dstamp), 'MINUTE') finish_time,
               SUM(s1.qty_ordered) qty_ordered,
               SUM(NVL(s1.qty_to_pick,0)) qty_to_pick,
               SUM(NVL(s1.qty_picked,0)) qty_picked,
               SUM(NVL(s1.qty_to_load,0)) qty_to_load,
               SUM(NVL(s1.to_load_pallets,0)) to_load_pallets,
               SUM(s1.qty_loaded) qty_loaded,
               SUM(s1.qty_shipped) qty_shipped,
               CASE WHEN MIN(shipped) > 0 then MAX(s1.shipped_date) END shipped_date,
               s1.planned_shipment_dstamp,
               MAX(s1.last_pick_dstamp) last_pick_dstamp
        FROM (
                SELECT bid.site_id,
                       bid.due_dstamp,
                       bid.start_slot,
                       bid.bookref_id,
                       bid.trailer_id,
                       ol.qty_ordered qty_ordered,
                       CASE WHEN EXISTS(SELECT NULL
                                        FROM shipping_manifest sm
                                        WHERE oh.client_id = sm.client_id
                                              AND oh.order_id = sm.order_id
                                              --AND ol.line_id = sm.line_id
                                              AND rownum = 1) THEN 'Y' ELSE 'N' END start_load,
                       CASE WHEN EXISTS(SELECT NULL
                                        FROM shipment_seals ss
                                        WHERE bid.trailer_id = ss.trailer_id
                                              AND oh.client_id = ss.client_id
                                              AND oh.consignment = ss.consignment) THEN 'Y' ELSE 'N' END sealed,
                       mt.qty_to_pick,
                       mt.qty_to_load,
                       mt.to_load_pallets,
                       mt.qty_picked,
                       NVL(ol.qty_picked,0) qty_loaded,
                       NVL(ol.qty_shipped,0) qty_shipped,
                       DECODE(oh.status,'Shipped',1,0) shipped,
                       oh.shipped_date,
                       oh.consignment,
                       c.shipment_dstamp planned_shipment_dstamp,
                       (SELECT MAX(itl.dstamp) last_
                        FROM inventory_transaction itl
                        WHERE     itl.code = 'Pick'
                              AND itl.site_id = $P{site_id}
                              AND itl.client_id = $P{client_id}
                              AND itl.client_id = oh.client_id
                              AND itl.reference_id = oh.order_id
                              AND itl.site_id = oh.from_site_id) last_pick_dstamp
                FROM booking_in_diary bid,
                     booking_in_diary_details bidd,
                     order_header oh,
                     order_line ol,
                     (SELECT mt.client_id,
                             mt.task_id,
                             mt.line_id,
                             SUM(CASE WHEN mt.status != 'Consol' THEN mt.qty_to_move ELSE 0 END) qty_to_pick,
                             SUM(CASE WHEN mt.status = 'Consol' then mt.qty_to_move ELSE 0 END) qty_picked,
                             SUM(CASE WHEN mt.from_loc_id = l.in_stage THEN mt.qty_to_move ELSE 0 END) qty_to_load,
                             COUNT(DISTINCT(CASE WHEN mt.from_loc_id = l.in_stage THEN mt.pallet_id END)) to_load_pallets
                      FROM move_task mt,
                           location l
                      WHERE     mt.site_id = l.site_id
                            AND mt.final_loc_id = l.location_id
                            AND mt.site_id = $P{site_id}
                            AND mt.client_id = $P{client_id}
                            AND mt.task_type = 'O'
                      GROUP BY mt.client_id,
                               mt.task_id,
                               mt.line_id
                      ) MT,
                     consignment c
                WHERE bid.bookref_id = bidd.bookref_id
                      AND ((bidd.type = 'Consignment' AND bidd.reference_id = oh.consignment) OR
                           (bidd.type = 'Order' AND bidd.reference_id = oh.order_id))
                      AND oh.client_id = ol.client_id
                      AND oh.order_id = ol.order_id
                      AND ol.client_id = mt.client_id (+)
                      AND ol.order_id = mt.task_id (+)
                      AND ol.line_id = mt.line_id (+)
                      AND oh.from_site_id = c.site_id
                      AND oh.consignment = c.consignment
                      AND bid.due_dstamp BETWEEN trunc(sysdate)-1 and trunc(sysdate) +1
                      AND bid.site_id = $P{site_id}
                      AND oh.from_site_id = $P{site_id}
                      AND oh.client_id = $P{client_id}
                      AND bid.booking_type = 'Out'
              ) S1
        GROUP BY s1.site_id,
                 s1.trailer_id,
                 s1.due_dstamp,
                 s1.start_slot,
                 s1.planned_shipment_dstamp,
                 s1.consignment
        ) S2,
       site s
WHERE      s2.site_id = s.site_id
      AND start_time between CASE
          WHEN SYSDATE > TRUNC (SYSDATE) + INTERVAL '6' HOUR
          THEN
             TRUNC (SYSDATE) + INTERVAL '6' HOUR
          ELSE
             TRUNC (SYSDATE - 1) + INTERVAL '6' HOUR
       END AND CASE
          WHEN SYSDATE > TRUNC (SYSDATE) + INTERVAL '6' HOUR
          THEN
             TRUNC (SYSDATE) + INTERVAL '30' HOUR
          ELSE
             TRUNC (SYSDATE) + INTERVAL '6' HOUR
       END
order by 1, 2, 3]]>
	</queryString>
	<field name="SITE_ID" class="java.lang.String"/>
	<field name="TRAILER_ID" class="java.lang.String"/>
	<field name="CONSIGNMENT" class="java.lang.String"/>
	<field name="START_TIME" class="oracle.sql.TIMESTAMPLTZ"/>
	<field name="FINISH_TIME" class="oracle.sql.TIMESTAMPLTZ"/>
	<field name="QTY_ORDERED" class="java.math.BigDecimal"/>
	<field name="QTY_TO_PICK" class="java.math.BigDecimal"/>
	<field name="QTY_PICKED" class="java.math.BigDecimal"/>
	<field name="QTY_TO_LOAD" class="java.math.BigDecimal"/>
	<field name="TO_LOAD_PALLETS" class="java.math.BigDecimal"/>
	<field name="QTY_LOADED" class="java.math.BigDecimal"/>
	<field name="QTY_SHIPPED" class="java.math.BigDecimal"/>
	<field name="SHIPPED_DATE" class="oracle.sql.TIMESTAMPLTZ"/>
	<field name="PLANNED_SHIPMENT_DSTAMP" class="oracle.sql.TIMESTAMPLTZ"/>
	<field name="LAST_PICK_DSTAMP" class="oracle.sql.TIMESTAMPLTZ"/>
	<field name="LOAD_STATUS" class="java.lang.String"/>
	<field name="SEAL_RAG" class="java.lang.String"/>
	<field name="PICK_RAG" class="java.lang.String"/>
	<field name="LOAD_RAG" class="java.lang.String"/>
	<field name="PLANNED_SHIP_TIME" class="java.lang.String"/>
	<field name="TIME_RUN" class="java.lang.String"/>
	<field name="NOTES" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="47" splitType="Stretch">
			<rectangle>
				<reportElement style="rptHeaderRectangle" x="0" y="0" width="782" height="46" uuid="1e729468-0bec-4278-8c85-4d5da00e2750"/>
			</rectangle>
			<staticText>
				<reportElement style="rptTitle" x="0" y="0" width="782" height="47" uuid="3368f65a-712e-4ddd-ad4c-06154ac90d6f"/>
				<text><![CDATA[Despatch Status]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="70" splitType="Stretch">
			<staticText>
				<reportElement style="NOSTYLE" x="0" y="27" width="100" height="43" uuid="e1f19476-a385-496c-be64-aa9cf0ba218d"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[TRAILER ID]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="100" y="27" width="100" height="43" uuid="37993eef-b144-4caa-b393-f16909296fef"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[CONSIGNMENT]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="200" y="27" width="100" height="43" uuid="712cfad5-b977-4e30-99c2-fd14949d2316"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ORDERED]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="300" y="27" width="75" height="43" uuid="21ae27e8-b825-43fb-862e-116d22ea1267"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[PICKED]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="406" y="27" width="81" height="43" uuid="c580de6a-8b95-48ff-8747-53b66f00c5f6"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[LOADED]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="549" y="27" width="50" height="43" uuid="c16b2bcc-fb79-441c-9269-85a6b5766c68"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[URNs to load]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="690" y="27" width="80" height="43" uuid="d3c41052-79b8-49ee-9d40-a185b52c3040"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[STATUS]]></text>
			</staticText>
			<staticText>
				<reportElement style="NOSTYLE" x="599" y="27" width="50" height="43" uuid="03c98f1b-ae38-4329-89f2-eda3a8eb4c7c"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[SEAL]]></text>
			</staticText>
			<textField>
				<reportElement style="NOSTYLE" x="676" y="0" width="100" height="40" uuid="c84ee77e-c54f-4816-9712-0f21c60fac1e"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{TIME_RUN}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="649" y="27" width="126" height="20" uuid="6d078ed4-6d29-4308-99f4-b4f17f0a7c18"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{NOTES}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="25" splitType="Stretch">
			<rectangle>
				<reportElement style="rptDetailRectangle" x="0" y="0" width="782" height="25" uuid="4c4c81ad-38fa-483e-8afb-5ff35a06d6b0">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() %2 == 0)]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField>
				<reportElement style="NOSTYLE" x="0" y="3" width="100" height="21" uuid="fb0b759d-f7f5-464e-bdbc-d038dc56c4c0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{TRAILER_ID}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="100" y="3" width="100" height="21" uuid="fe35ddc5-beed-4dcd-9743-2093bfd74e81"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{CONSIGNMENT}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="200" y="3" width="100" height="21" uuid="0cc844f9-f9a1-4ac5-90aa-e8f9b393448b"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{QTY_ORDERED}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="300" y="3" width="75" height="21" uuid="ef5b2a72-5f52-4f87-b7a7-112e006b3e2a"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{QTY_PICKED}]]></textFieldExpression>
			</textField>
			<image>
				<reportElement style="NOSTYLE" x="374" y="3" width="26" height="21" uuid="a342171b-0c6e-4430-944d-8821cfe51a46">
					<printWhenExpression><![CDATA[new Boolean($F{PICK_RAG}.equals( "R") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_red.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="374" y="3" width="26" height="21" uuid="2a32754c-1cb4-4f8d-8b8e-c7e24d54cd77">
					<printWhenExpression><![CDATA[new Boolean($F{PICK_RAG}.equals( "B") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_blue.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="374" y="3" width="26" height="21" uuid="38cdb2f8-d16d-4898-8d01-1ef34e9b0e54">
					<printWhenExpression><![CDATA[new Boolean($F{PICK_RAG}.equals( "G") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_green.jpg"]]></imageExpression>
			</image>
			<textField>
				<reportElement mode="Transparent" x="374" y="3" width="26" height="21" uuid="afce8289-db2a-4d9e-be05-ce4bb94b9f90">
					<printWhenExpression><![CDATA[new Boolean($F{PICK_RAG}.equals( "?") )]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PICK_RAG}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="406" y="3" width="81" height="21" uuid="b4fa2a6b-8f91-4b30-9146-2d95b24b96f8"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{QTY_LOADED}]]></textFieldExpression>
			</textField>
			<image>
				<reportElement style="NOSTYLE" x="488" y="3" width="26" height="21" uuid="2209089a-a712-4577-8cbe-c005852f00f8">
					<printWhenExpression><![CDATA[new Boolean($F{LOAD_RAG}.equals( "R") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_red.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="488" y="3" width="26" height="21" uuid="aa1a7cc3-848e-4cfb-b9f8-d0a01011d205">
					<printWhenExpression><![CDATA[new Boolean($F{LOAD_RAG}.equals( "G") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_green.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="488" y="3" width="26" height="21" uuid="3ca4fc1c-60a7-4205-9e6d-26b14245d324">
					<printWhenExpression><![CDATA[new Boolean($F{LOAD_RAG}.equals( "B") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_blue.jpg"]]></imageExpression>
			</image>
			<textField>
				<reportElement style="NOSTYLE" x="488" y="3" width="23" height="21" uuid="f1b6955e-a52c-45b4-be66-4e0ebaf606f8">
					<printWhenExpression><![CDATA[new Boolean($F{LOAD_RAG}.equals( "?") )]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LOAD_RAG}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="549" y="3" width="50" height="21" uuid="69121f8a-c328-4b9f-b3a2-7738958a3a56"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{TO_LOAD_PALLETS}]]></textFieldExpression>
			</textField>
			<image>
				<reportElement style="NOSTYLE" x="655" y="3" width="26" height="21" uuid="f6665a6b-1835-472a-ba93-11893ce9cbe3">
					<printWhenExpression><![CDATA[new Boolean($F{SEAL_RAG}.equals( "R") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_red.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="655" y="3" width="26" height="21" uuid="2c838af5-eddc-49e7-bb73-13024d6a111c">
					<printWhenExpression><![CDATA[new Boolean($F{SEAL_RAG}.equals( "A") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_amber.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="655" y="3" width="26" height="21" uuid="ff93cb1b-64ef-468a-8a16-79228d6aea70">
					<printWhenExpression><![CDATA[new Boolean($F{SEAL_RAG}.equals( "B") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_blue.jpg"]]></imageExpression>
			</image>
			<image>
				<reportElement style="NOSTYLE" x="655" y="3" width="26" height="21" uuid="07b76149-9dc4-41e1-ab8c-eec11638962f">
					<printWhenExpression><![CDATA[new Boolean($F{SEAL_RAG}.equals( "G") )]]></printWhenExpression>
				</reportElement>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/led_green.jpg"]]></imageExpression>
			</image>
			<textField>
				<reportElement style="NOSTYLE" x="655" y="3" width="21" height="21" uuid="98aa6702-e099-4d90-b22f-9b3a0a019e22">
					<printWhenExpression><![CDATA[new Boolean($F{SEAL_RAG}.equals( "?") )]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SEAL_RAG}]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement style="NOSTYLE" x="599" y="3" width="52" height="21" uuid="6ed285ac-dd68-41f7-b8e4-dc31226a466d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{PLANNED_SHIP_TIME}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="NOSTYLE" x="690" y="3" width="80" height="21" uuid="53a4925c-3ca7-4b07-843e-e63da67296fa"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{LOAD_STATUS}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="NOSTYLE" x="407" y="4" width="42" height="20" uuid="5fd6c41b-3d4c-49b7-8a1b-ec37937e753a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="NOSTYLE" x="204" y="4" width="202" height="20" uuid="479a2e7f-9401-4305-ac45-d95a252d2428"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
