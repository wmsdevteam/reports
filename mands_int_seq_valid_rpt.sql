DECLARE
/******************************************************************************************/
/*                                                                                        */
/*  NAME:           mands_int_seq_valid_rpt.sql                                           */
/*                                                                                        */
/*  DESCRIPTION:    As part of delivering the I2346 interface an element of sequence      */
/*                  checking is required to validate all messages have been recieved from */
/*                  Q > CXM. GroupKey, GroupSequence, KeyFlag will be passed to provide   */
/*                  this functionality                                                    */
/*                                                                                        */
/*   DATE     REFERENCE   BY          DESCRIPTION                                         */
/*   ======== =========== =========== ================================================    */
/*   01/05/18 JDA-10897   Lee S       Initial version                                     */
/*                                                                                        */
/******************************************************************************************/

c_Failure CONSTANT Integer := 0;
c_MQSLevelFatal              CONSTANT Integer := 1;
c_MQSLevelError              CONSTANT Integer := 2;
c_MQSLevelWarning            CONSTANT Integer := 3;
c_MQSLevelDebug              CONSTANT Integer := 4;
c_MQSLevelInfo               CONSTANT Integer := 5;

p_SiteID dcsdba.site.site_id%TYPE:= '&1';
v_Curr_Seq mands.wsl_sequence.group_sequence%TYPE:= 0;
v_Miss_Seq CLOB:= NULL;
v_H_Last_Seq VARCHAR(25):= NULL;
v_L_Last_Seq VARCHAR(25):= NULL;
v_Error INTEGER:= 0;
v_EscapeChar VARCHAR(2):= CHR(13);
v_TabChar VARCHAR(1):= CHR(9);
v_LastMsgDstamp INTEGER := dcsdba.LibSystemProfile.GetNumber('-ROOT-_USER_REPORTING_MANDSINTSEQVALIDRPT_' || p_SiteID || '_DELAY');
v_FileName   Varchar2(50);
v_OUTTRAY    SYS.all_directories.directory_name%TYPE := 'INTERFACE';
v_OUTWORK    SYS.all_directories.directory_name%TYPE := 'OUTWORK';
v_OUTREJECT  SYS.all_directories.directory_name%TYPE := 'OUTREJECT';
Result      Integer;
invalid_directory   EXCEPTION;
write_to_file_error EXCEPTION;

CURSOR groupKeySearch IS 
SELECT wsls.site_id, 
       wsls.interface_id, 
       wsls.group_key, 
       MIN(CASE WHEN wsls.interface_type = 'mergeOrderHeader' THEN wsls.Group_Sequence END) SHSEQ, 
       MAX(CASE WHEN wsls.interface_type = 'mergeOrderHeader' THEN wsls.Group_Sequence END) EHSEQ , 
       MIN(CASE WHEN wsls.interface_type = 'mergeOrderLine' THEN wsls.Group_Sequence END) SLSEQ, 
       MAX(CASE WHEN wsls.interface_type = 'mergeOrderLine' THEN wsls.Group_Sequence END) ELSEQ
FROM mands.wsl_sequence wsls
WHERE wsls.uploaded = 'N'
AND (wsls.group_flag = 'LAST' OR (wsls.group_flag != 'LAST' AND wsls.dstamp < sysdate - 1)) 
AND wsls.site_id = p_SiteID
GROUP BY wsls.site_id, wsls.interface_id, wsls.group_key
ORDER BY wsls.group_key;

CURSOR groupKeyLineHeaderSearch (GKEY mands.wsl_sequence.key%TYPE,
                                 SITE mands.wsl_sequence.site_id%TYPE)
IS
SELECT wsls.group_sequence
FROM mands.wsl_sequence wsls
WHERE wsls.group_key = GKEY
AND wsls.site_id = SITE
AND wsls.interface_type = 'mergeOrderHeader'
ORDER BY wsls.group_sequence
FOR UPDATE OF wsls.uploaded_filename,
              wsls.uploaded_dstamp,
              wsls.uploaded;

CURSOR groupKeyLineLineSearch (GKEY mands.wsl_sequence.key%TYPE,
                               SITE mands.wsl_sequence.site_id%TYPE)
IS
SELECT wsls.group_sequence
FROM mands.wsl_sequence wsls
WHERE wsls.group_key = GKEY
AND wsls.site_id = SITE
AND wsls.interface_type = 'mergeOrderLine'
ORDER BY wsls.group_sequence
FOR UPDATE OF wsls.uploaded_filename,
              wsls.uploaded_dstamp,
              wsls.uploaded;

BEGIN

    dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Start',4);
    dcsdba.LibMQSDebug.SetSessionID(USERENV('SESSIONID'),'PLSQL','MANDS_INT_SEQ_VALID_RPT');
    dcsdba.LibMQSDebug.SetDebugLevel(dcsdba.LibSystemProfile.GetNumber('-ROOT-_USER_LOGGING_MANDSINTSEQVALIDRPT'));

    Result := c_Failure;
    /*
    ||Make sure the directories exist
    */
    IF mands.Lib_Extracts.DirectoryExists(v_OUTTRAY) = FALSE THEN
        RAISE invalid_directory;
    ELSIF mands.Lib_Extracts.DirectoryExists(v_OUTWORK) = FALSE THEN
        RAISE invalid_directory;
    ELSIF mands.Lib_Extracts.DirectoryExists(v_OUTREJECT) = FALSE THEN
        RAISE invalid_directory;
    END IF;

FOR groupKeyRow IN groupKeySearch LOOP

  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Working on Group Key ['||groupkeyRow.Group_Key||']',4);
  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Group Key (Header) [' || groupkeyRow.Group_Key || '] - Min ['||groupkeyRow.SHSEQ||'] Max [' || groupkeyRow.EHSEQ || ']',4);
  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Group Key (Header) [' || groupkeyRow.Group_Key || '] - Min ['||groupkeyRow.SLSEQ||'] Max [' || groupkeyRow.ELSEQ || ']',4);
  
  BEGIN      
        SELECT mwsl.Group_Sequence INTO v_H_Last_Seq
        FROM mands.wsl_sequence mwsl
        WHERE mwsl.Group_key = groupKeyRow.Group_Key
        AND mwsl.Group_Flag = 'LAST'
        AND mwsl.interface_type = 'mergeOrderHeader'
        AND mwsl.site_id = p_SiteID
        GROUP BY mwsl.Group_Sequence;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
        v_H_Last_Seq:='NO LAST FLAG';
        v_Error:= 1;
        WHEN TOO_MANY_ROWS THEN 
        v_H_Last_Seq:='MULTIPLE LAST FLAGS';
        v_Error:= 1;
        WHEN OTHERS THEN
        v_Error:= 1;   
  END;
  
  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - v_H_Last_Seq [' || v_H_Last_Seq || ']',4);
  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Calculating last sequence for Group Key (Line) [' || groupkeyRow.Group_Key || ']',4);
  BEGIN
        SELECT mwsl.Group_Sequence INTO v_L_Last_Seq
        FROM mands.wsl_sequence mwsl
        WHERE mwsl.Group_key = groupKeyRow.Group_Key
        AND mwsl.Group_Flag = 'LAST'
        AND mwsl.interface_type = 'mergeOrderLine'
        AND mwsl.site_id = p_SiteID
        GROUP BY mwsl.Group_Sequence;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
        v_L_Last_Seq:='NO LAST FLAG';
        v_Error:= 1;
        WHEN TOO_MANY_ROWS THEN 
        v_L_Last_Seq:='MULTIPLE LAST FLAGS';
        v_Error:= 1;
        WHEN OTHERS THEN
        v_Error:= 1;  
  END;
  
  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - v_L_Last_Seq [' || v_L_Last_Seq || ']',4);
  dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Checking Group Key header sequence...',4);
  
  FOR groupKeyLineHeaderRow IN groupKeyLineHeaderSearch(groupKeyRow.group_key, groupKeyRow.site_id) LOOP
       
     IF v_Curr_Seq + 1 != groupKeyLineHeaderRow.group_sequence THEN
    
        LOOP  
        
        v_Curr_Seq := v_Curr_Seq + 1;
        
        EXIT WHEN v_Curr_Seq + 1 = groupKeyLineHeaderRow.group_sequence; 
        
        v_Miss_Seq:= v_Miss_Seq || ';' || to_CHAR( v_Curr_Seq + 1 );
        
        END LOOP;

     END IF;
     
  END LOOP;

IF v_Miss_Seq IS NOT NULL 
OR groupkeyRow.SHSEQ || groupkeyRow.EHSEQ IS NULL
OR groupkeyRow.SLSEQ || groupkeyRow.EHSEQ IS NULL
OR v_Error = 1 THEN

    v_FileName :=  'MISR.' || groupKeyRow.site_id || '.'|| groupKeyRow.interface_id || '.' || groupKeyRow.Group_Key|| '.txt';
    dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Generating filename ['||v_FileName||']',4);
    
    IF v_Miss_Seq IS NOT NULL THEN
    
    Lib_Extracts.ClobToFile(v_OUTWORK,v_FileName,
    groupKeyRow.interface_id || ' - ' || p_SiteID || ' - ' || systimestamp || v_EscapeChar ||
    'Group Key [' || groupKeyRow.Group_Key || ']' || v_EscapeChar || 
    'Min Header Group Sequence' || v_TabChar || '[' || groupkeyRow.SHSEQ || ']' || v_TabChar || 'Max Header Group Sequence' || v_TabChar || '[' || groupkeyRow.EHSEQ || ']' || v_TabChar || 'Header Last Message' || v_TabChar || '[' || v_H_Last_Seq || ']' || v_EscapeChar ||
    'Min Line Group Sequence'   || v_TabChar || '[' || groupkeyRow.SLSEQ || ']' || v_TabChar || 'Max Line Group Sequence'   || v_TabChar || '[' || groupkeyRow.ELSEQ || ']' || v_TabChar || 'Line Last Message'   || v_TabChar || '[' || v_L_Last_Seq || ']' || v_EscapeChar ||
    'Missing Header Sequence(s):' || v_EscapeChar ||'[' || v_Miss_Seq|| ']'
    , Result);
    
    ELSE
    
    Lib_Extracts.ClobToFile(v_OUTWORK,v_FileName,
    groupKeyRow.interface_id || ' - ' || p_SiteID || ' - ' || systimestamp || v_EscapeChar ||
    'Group Key [' || groupKeyRow.Group_Key || ']' || v_EscapeChar ||
    'Min Header Group Sequence' || v_TabChar || '[' || groupkeyRow.SHSEQ || ']' || v_TabChar || 'Max Header Group Sequence' || v_TabChar || '[' || groupkeyRow.EHSEQ || ']' || v_TabChar || 'Header Last Message' || v_TabChar || '[' || v_H_Last_Seq || ']' || v_EscapeChar ||
    'Min Line Group Sequence'   || v_TabChar || '[' || groupkeyRow.SLSEQ || ']' || v_TabChar || 'Max Line Group Sequence'   || v_TabChar || '[' || groupkeyRow.ELSEQ || ']' || v_TabChar || 'Line Last Message'   || v_TabChar || '[' || v_L_Last_Seq || ']' || v_EscapeChar ||
    'No Missing Header Sequence(s)' || v_EscapeChar
    , Result);
  
    END IF;
    
    IF Result = c_Failure THEN
       RAISE write_to_file_error;
    END IF;
    
    UPDATE mands.wsl_sequence wsl
    SET wsl.uploaded_filename = v_FileName,
        wsl.uploaded_dstamp = SYSTIMESTAMP,
        wsl.uploaded = NULL
    WHERE wsl.Group_Key = groupKeyRow.Group_Key
    AND wsl.site_id = p_SiteID
    AND wsl.interface_type = 'mergeOrderHeader';
    
ELSE

    dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - No Headers to extract - setting uploaded filename to ''N/A''',4);

    UPDATE mands.wsl_sequence wsl
    SET wsl.uploaded_filename = 'N/A',
        wsl.uploaded_dstamp = SYSTIMESTAMP,
        wsl.uploaded = NULL
    WHERE wsl.Group_Key = groupKeyRow.Group_Key
    AND wsl.site_id = p_SiteID    AND wsl.interface_type = 'mergeOrderHeader';

END IF;

COMMIT;

v_Miss_Seq:= NULL;
v_Curr_Seq:= 0;
v_Error:= 0;

dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Checking Group Key line sequence...',4);

FOR groupKeyLineLineRow IN groupKeyLineLineSearch(groupKeyRow.group_key, groupKeyRow.site_id) LOOP
  
     IF v_Curr_Seq + 1 != groupKeyLineLineRow.group_sequence THEN
     
        LOOP  
        
        v_Curr_Seq := v_Curr_Seq + 1;
        
        EXIT WHEN v_Curr_Seq + 1 = groupKeyLineLineRow.group_sequence; 
        
        v_Miss_Seq:= v_Miss_Seq || ';' || to_CHAR( v_Curr_Seq + 1 );
        
        END LOOP;

     END IF;
    
  END LOOP;      

IF v_Miss_Seq IS NOT NULL THEN

    IF v_FileName IS NULL THEN
    v_FileName :=  'MISR.' || groupKeyRow.site_id || '.'|| groupKeyRow.interface_id || '.' || groupKeyRow.Group_Key|| '.txt';
    dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Generating filename ['||v_FileName||']',4);         
    
    Lib_Extracts.ClobToFile(v_OUTWORK,v_FileName,
    groupKeyRow.interface_id || ' - ' || p_SiteID || ' - ' || systimestamp || v_EscapeChar ||
    'Group Key [' || groupKeyRow.Group_Key || ']' || v_EscapeChar || v_TabChar || v_TabChar ||  'Min Header Group Sequence [' || groupkeyRow.SHSEQ || '] Max Header Group Sequence [' || groupkeyRow.EHSEQ || '] Header Last Message [' || v_H_Last_Seq || ']' || v_EscapeChar || v_TabChar || v_TabChar ||  'Min Line Group Sequence [' || groupkeyRow.SLSEQ || '] Max Line Group Sequence [' || groupkeyRow.ELSEQ || '] Line Last Message [' || v_L_Last_Seq || ']' || v_EscapeChar ||
    'Min Header Group Sequence' || v_TabChar || '[' || groupkeyRow.SHSEQ || ']' || v_TabChar || 'Max Header Group Sequence' || v_TabChar || '[' || groupkeyRow.EHSEQ || ']' || v_TabChar || 'Header Last Message' || v_TabChar || '[' || v_H_Last_Seq || ']' || v_EscapeChar ||
    'Min Line Group Sequence'   || v_TabChar || '[' || groupkeyRow.SLSEQ || ']' || v_TabChar || 'Max Line Group Sequence'   || v_TabChar || '[' || groupkeyRow.ELSEQ || ']' || v_TabChar || 'Line Last Message'   || v_TabChar || '[' || v_L_Last_Seq || ']' || v_EscapeChar ||
    'No Missing Header Sequence(s)' || v_EscapeChar ||
    'Missing Line Sequence(s):' || v_EscapeChar || '[' || v_Miss_Seq|| ']'
    ,Result);
    
    ELSE
    
    Lib_Extracts.ClobToFile(v_OUTWORK,v_FileName,
    'Missing Line Sequence(s):' || v_EscapeChar || '[' || v_Miss_Seq|| ']'
    ,Result);
    
    END IF;
    
      IF Result = c_Failure THEN
         RAISE write_to_file_error;
      END IF;
    
    UPDATE mands.wsl_sequence wsl
    SET wsl.uploaded_filename = v_FileName,
        wsl.uploaded_dstamp = SYSTIMESTAMP,
        wsl.uploaded = NULL
    WHERE wsl.Group_Key = groupKeyRow.Group_Key
    AND wsl.site_id = p_SiteID
    AND wsl.interface_type = 'mergeOrderLine';
    
ELSE

    IF v_FileName IS NULL THEN
    
    dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - No Lines to extract - setting uploaded filename to ''N/A''',4);
    
    UPDATE mands.wsl_sequence wsl
    SET wsl.uploaded_filename = 'N/A',
        wsl.uploaded_dstamp = SYSTIMESTAMP,
        wsl.uploaded = NULL
    WHERE wsl.Group_Key = groupKeyRow.Group_Key
    AND wsl.site_id = p_SiteID
    AND wsl.interface_type = 'mergeOrderLine';
    
    ELSE
    
    Lib_Extracts.ClobToFile(v_OUTWORK,v_FileName,
    'No Missing Line Sequence(s)'
    , Result);
    
    IF Result = c_Failure THEN
       RAISE write_to_file_error;
    END IF;
    
    UPDATE mands.wsl_sequence wsl
    SET wsl.uploaded_filename = v_FileName,
        wsl.uploaded_dstamp = SYSTIMESTAMP,
        wsl.uploaded = NULL
    WHERE wsl.Group_Key = groupKeyRow.Group_Key
    AND wsl.site_id = p_SiteID
    AND wsl.interface_type = 'mergeOrderLine';
    
    END IF;
    
END IF;

COMMIT;

UTL_FILE.FRENAME(v_OUTWORK, /*Directory where file currently lives*/ v_FileName, /*File that we're moving*/ v_OUTTRAY, /*Directory where we are moving the file to*/ v_FileName, /*Name of the file we're creating in dest dir*/ FALSE /*Overwrite file if already exists?*/ );

v_Miss_Seq:= NULL;
v_Curr_Seq:= 0;
v_FileName:= NULL;
v_H_Last_Seq:= NULL;
v_L_Last_Seq:= NULL;

END LOOP;

dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - End',4);

EXCEPTION
  WHEN invalid_directory THEN
        dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - The directory does not exist',c_MQSLevelError);
        ROLLBACK;
  WHEN OTHERS THEN
        ROLLBACK;
        dcsdba.LibError.WriteErrorLog('mands.Lib_Extracts.InterfaceSeqValidation', SQLCODE,SUBSTR(SQLERRM,1,300));
        dcsdba.LibMQSDebug.Print('mands.Lib_Extracts.InterfaceSeqValidation - Error ['||SQLERRM||'] Back Trace ['|| DBMS_UTILITY.FORMAT_ERROR_BACKTRACE||']',c_MQSLevelError);
        ROLLBACK;

END;